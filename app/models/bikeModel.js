var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt-nodejs');

// Bike schema
var BikeSchema = new Schema({

  model: { type: String },
  make: {type: String},
  creator: {type: String},
  _id: {type: String},
  manufacturer: {type: String},
  model: {type: String},
  year_of_build: {type: String},
  exhaust:{type:String},
  category: {type: String},
  year_of_rebuild: {type: String},
  frame_mods: {type: String},
  body_work: {type: String},
  motor: {type: String},
  frontSus: {type: String},
  rearSus: {type: String},
  wheels: {type: String},
  brakes: {type: String},
  hand_controls: {type: String},
  paint: {type: String},
  gas_tank: {type: String},
  forks: {type: String},
  shoks: {type: String},
  imgs : [],
  description :{type: String },

  thumb:{type:String}

});



module.exports = mongoose.model('Bike', BikeSchema);
