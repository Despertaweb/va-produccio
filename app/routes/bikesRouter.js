const util = require('util');
var express = require('express');
var bikesRouter = express.Router(); // get an instance of the express router
var Bike = require('../models/bikeModel.js');
var multiparty = require('multiparty');
var fs = require('fs');
var cloudinary = require('cloudinary');
var Verify = require('./verify.js')



cloudinary.config({ 
  cloud_name: 'www-despertaweb-cat', 
  api_key: '871184283322657', 
  api_secret: 'dZMKhXuHdemVwFBhEf7CmREDbVs' 
});

bikesRouter.route('/bikes')

    //GET ALL
	.get(Verify.verifyOrdinaryUser, function(req, res, next) {
        Bike.find({}, function(err, bikes) {
            if (err) res.send(err);
            res.json(bikes);
        })
    })


	//Create  BIKE
	.post(Verify.verifyOrdinaryUser, function(req, res, next) {
	    var form = new multiparty.Form();
	    console.log('\n\nSAVING ....\n', req.body);
	 
	        Bike.create(req.body, function(err, newBike) {
	            if (err) console.log('ERROR ' + err);
	            else  res.end('New Bike ',newBike);

	        });

	    });
	;



/******* AUTHENTICATED ROUTES api & admin  ********/

/*******   routes that en in /bikes/:id    ********/

bikesRouter.route('/bikes/:id')

.get(Verify.verifyOrdinaryUser, function(req, res) {
    Bike.findById(req.params.id, function(err, bike) {
        if (err) res.send(err);
        console.log('GET BIKE : \n' + bike);
        // return that bike
        res.json(bike);
    });
})



.put(Verify.verifyOrdinaryUser, function(req, res) {
    Bike.findOneAndUpdate(
        {_id: req.params.id}, // find a document with that filter
        req.body, // document to insert when nothing was found
        {upsert: true, new: true, runValidators: true}, // options
        function (err, updatedBike) { // callback
    		if (err) console.log('ERROR '+ err);
         	else res.json(updatedBike)

        }
    );

}) // /PUT



.delete(Verify.verifyOrdinaryUser, function(req,res) {

	// res.send('HELLO DELETE '+ req.params.id)
	Bike.findByIdAndRemove(req.params.id, function (err, bike) {  
	    // We'll create a simple object to send back with a message and the id of the document that was removed
	    // You can really do this however you want, though.
	    var response = {
	        message: "Bike successfully Deleted",
	        id: bike._id
	    };
	    res.send(response);
	})

})


;










module.exports = bikesRouter;