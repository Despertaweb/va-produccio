var express = require('express');
var router = express.Router();
var passport = require('passport');
var User = require('../models/userModel.js');
var Verify    = require('./verify');

/* GET users listing. */
router.get('/', function(req, res, next) {

  User.find({}, function(err, users) {
            if (err) res.send(err);
            res.json(users);
        })
  
});

router.post('/register', function(req, res) {


    User.register(new User({ username : req.body.username }),
        req.body.password,
       function(err, user) {
        if (err) {
            return res.status(500).json({err: err});
        }
        passport.authenticate('local')(req, res, function () {
            return res.status(200).json({status: 'Registration Successful!'});
        });
    });
});

router.post('/login', function(req, res, next) {



  passport.authenticate('local', function(err, user, info) {
    console.log('\nerr:', err);
  console.log('\nUSER:', user);
  console.log('\ninfo:', info);
    if (err) {
      return next(err);
    }
    if (!user) {
      return res.status(401).json({
        err: info.message
      });
    }
    req.logIn(user, function(err) {
      if (err) {
        console.log('ERR',err);
        return res.status(500).json({
          err: 'Could not log in user'
        });
      }
        
      var token = Verify.getToken(user);
        res.status(200).json({
          status: 'Login successful!',
          success: true,
          token: token
        });
    });
  })(req,res,next);
});

router.get('/logout', function(req, res) {
  console.log('S\'ha de fer token destroy aqui');
  req.logout();
  res.status(200).json({
    status: 'Bye!'
  });
});

module.exports = router;