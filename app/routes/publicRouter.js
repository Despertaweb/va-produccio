var express = require('express');
var publicRouter = express.Router(); // get an instance of the express router
var Bike = require('../models/bikeModel.js');
var Car = require('../models/carModel.js');
var cloudinary = require('cloudinary');



cloudinary.config({ 
  cloud_name: 'www-despertaweb-cat', 
  api_key: '871184283322657', 
  api_secret: 'dZMKhXuHdemVwFBhEf7CmREDbVs' 
});


/*=======================================

	PUBLIC ROUTES 

=======================================*/
publicRouter.route('/bikes')

    //GET ALL
	.get( function(req, res, next) {
        Bike.find({}, function(err, bikes) {
            if (err) res.send(err);
            res.json(bikes);
        })
    })
;


publicRouter.route('/bikes/:id')

.get( function(req, res) {
    Bike.findById(req.params.id, function(err, bike) {
        if (err) res.send(err);
        console.log('GET BIKE : \n' + bike);
        // return that bike
        res.json(bike);
    });
});

/*=======================================*/
publicRouter.route('/cars')

    //GET ALL
	.get( function(req, res, next) {
        Car.find({}, function(err, cars) {
            if (err) res.send(err);
            res.json(cars);
        })
    })
;


publicRouter.route('/cars/:id')

.get( function(req, res) {
    Car.findById(req.params.id, function(err, car) {
        if (err) res.send(err);
        console.log('GET CAR : \n' + car);
        // return that car
        res.json(car);
    });
});




module.exports = publicRouter;