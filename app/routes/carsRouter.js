const util = require('util');
var express = require('express');
var carsRouter = express.Router(); // get an instance of the express router
var Car = require('../models/carModel.js');
var multiparty = require('multiparty');
var fs = require('fs');
var cloudinary = require('cloudinary');
var Verify = require('./verify.js')



cloudinary.config({ 
  cloud_name: 'www-despertaweb-cat', 
  api_key: '871184283322657', 
  api_secret: 'dZMKhXuHdemVwFBhEf7CmREDbVs' 
});




/******* AUTHENTICATED ROUTES api & admin  ********/



carsRouter.route('/cars')

    //GET ALL
    .get(Verify.verifyOrdinaryUser,  function(req, res, next) {
        Car.find({}, function(err, cars) {
            if (err) res.send(err);
            res.json(cars);
        })
    })


	//Create  CAR
	// .post(Verify.verifyOrdinaryUser, function(req, res, next) {
	.post(Verify.verifyOrdinaryUser, function(req, res, next) {
	    var form = new multiparty.Form();
	    console.log('\n\nSAVING ....\n', req.body);
	
        Car.create(req.body, function(err, newCar) {
            if (err) console.log('ERROR ' + err);
            else  res.end('New Car ',newCar);

        });

    

    });





/*******   routes that en in /cars/:id    ********/

carsRouter.route('/cars/:id')

.get(Verify.verifyOrdinaryUser, function(req, res) {
    Car.findById(req.params.id, function(err, car) {
        if (err) res.send(err);
        console.log('CAR : \n' + car);
        // return that car
        res.json(car);
    });




})



.put(Verify.verifyOrdinaryUser, function(req, res) {
    Car.findOneAndUpdate(
        {_id: req.params.id}, // find a document with that filter
        req.body, // document to insert when nothing was found
        {upsert: true, new: true, runValidators: true}, // options
        function (err, updatedCar) { // callback
    		if (err) console.log('ERROR '+ err);
         	else res.json(updatedCar)

        }
    );

}) // /PUT



.delete(Verify.verifyOrdinaryUser, function(req,res) {


// res.send('HELLO DELETE '+ req.params.id)
	Car.findByIdAndRemove(req.params.id, function (err, car) {  
	    // We'll create a simple object to send back with a message and the id of the document that was removed
	    // You can really do this however you want, though.
	    var response = {
	        message: "Car successfully Deleted",
	        id: car._id
	    };
	    res.send(response);
	})

})


;






module.exports = carsRouter;