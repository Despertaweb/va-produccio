  // BASE SETUP
  // ======================================

// CALL THE PACKAGES --------------------
var express    = require('express'); // call express
var app        = express(); // define our app using express
var bodyParser = require('body-parser'); // get body-parser
var morgan     = require('morgan'); // used to see requests
var path = require('path');
var cookieParser = require('cookie-parser');
var mongoose   = require('mongoose'); // for working w/ our database
    mongoose.Promise = require("bluebird"); // mongoose promises
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;





var config = require ('./config');

// ===============================
// DATABASE  connect to our database (hosted on mlab.com)
// ===============================

  mongoose.connect(config.database);
  var db = mongoose.connection;
  db.on('error', console.error.bind(console, 'connection error:'));
  db.once('open', function () {
      // we're connected!
      console.log("Connected correctly to Database Server\n\n");
  });

// ===============================
 // APP CONFIGURATION ---------------------
 // use body parser so we can grab information from POST requests
 // ===============================

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

// configure our app to handle CORS requests
 app.use(function(req, res, next) {
   res.setHeader('Access-Control-Allow-Origin', '*');
   res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
   res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, \
 Authorization');
   next();
});
 
 // log all requests to the console 
 app.use(morgan('dev'));


// passport config
var User = require('./app/models/userModel.js');
app.use(passport.initialize());
passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());


// set static files location
// used for requests that our frontend will make
  app.use(express.static(__dirname + '/public'));


// =============================
// API ROUTES                  |
// =============================
// all of our routes will be prefixed with /api


var publicRouter = require('./app/routes/publicRouter');
app.use('/public', publicRouter); 



var apiRouter = require('./app/routes/usersRouter');
app.use('/api', apiRouter);

var bikesRouter = require('./app/routes/bikesRouter');
app.use('/api', bikesRouter); 

var carsRouter = require('./app/routes/carsRouter');
app.use('/api', carsRouter); 





// MAIN CATCHALL ROUTE --------------- 
// SEND USERS TO FRONTEND ------------
// has to be registered after API ROUTES : IMPORTANT!
  app.get('*', function(req, res) {
    res.sendFile(path.join(__dirname + '/public/index.html'));
  });




// START THE SERVER
// ===============================
app.listen(config.port);
console.log('\033[2J');
console.log('|-------------------------------------------------------------------|\n| Magic happens on http://localhost:' + config.port+'\t\t\t    |'); 
console.log('| Database on: ' + config.database+'  |\n|-------------------------------------------------------------------|\n\n'); 



