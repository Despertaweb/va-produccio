'use stric';
angular.module('front.routes',['ui.router'])

.run(function($rootScope, $state, $timeout) {

  $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){
        
    // Seo Stuff
        var titles = {
                     'home':'CUSTOM BIKES & CARS Taller Barcelona',
                     'bikes':'Motos Custom',
                     'cars':'Coches Custom',
                     'garage':'Nuestro Taller',
                     'media':'Notícias y Media: Hablan de V/A!',
                     'contact':'Contacta con el equipo de V/A'
        };
        $rootScope.title = titles[toState.menu];
        var pag =toState.menu ;
        if (pag =='home' || 'bikes' || 'cars' || 'garage' || 'contact' || 'media') {
          $rootScope.noIndexPag = false;
                        
        } else $rootScope.noIndexPag = true;




        // if(toState.name !== 'login' && !UsersService.getCurrentUser()) {
        // event.preventDefault();
        // $state.go('login');
        // }
    });




 $rootScope.$on('$viewContentLoaded', function() {
       $('.carousel').carousel({
         interval: 3500,
         pause: false //changes the speed
       });

     // scroll body to 0px on click
       $('#back-to-top').click(function () {
           $('#back-to-top').tooltip('hide');
           $('body,html').animate({
               scrollTop: 0
           }, 800);
           return false;
       });


  



 });  // rootscope.on



 })




  .config(function ($stateProvider, $urlRouterProvider,$locationProvider,$httpProvider) {

   

    $stateProvider
      .state('home',{
        url:'/',
        menu:'home',

/** HOME **/

        views:{
          'header':{
            templateUrl:'app/views/home/home.header.html',
            controller:'fHomeCtrl'

          },
          'footer':{
            templateUrl:'app/views/home/home.footer.html'
          }
        }


      })

/**************************************************************************************/
/**************************************************************************************/
/***************************              BIKES       ********************************/
/**************************************************************************************/

      .state('home.bikes',{
        menu:'bikes',
        url:'bikes',
        views:{
          'header@':{
            templateUrl:'app/views/header.html',
            controller:'fmainController'
          },
          'content@':{
            templateUrl:'app/views/bikes/bikes.html',
            controller:'fBikesCtrl'
          },
          'footer@':{
            templateUrl:'app/views/footer.html'
          }
        },
        onEnter:function () {return stickNavBar(); },
        onExit: function() { $("#nav-general").unstick();}
      })


      .state('home.bike',{
        menu:'bikes',
        url:'bikes/:id',
        views:{
          'header@':{
            templateUrl:'app/views/header.html',
            controller:'fmainController'
          },
          'content@':{
            templateUrl:'app/views/bikes/bike.html',
            controller:'fBikeCtrl'
          },
          'footer@':{
            templateUrl:'app/views/footer.html'
          }
        },
        onEnter:function () {return stickNavBar(); },
        onExit: function() { $("#nav-general").unstick();}
      })


/***************************************************************/
/*                    CARS                                     */
/***************************************************************/
       .state('home.cars',{
        menu:'cars',
        url:'cars',
        views:{
          'header@':{
            templateUrl:'app/views/header.html',
            controller:'fmainController'
          },
          'content@':{
            templateUrl:'app/views/cars/cars.html',
            controller:'fCarsCtrl'
          },
          'footer@':{
            templateUrl:'app/views/footer.html'
          }
        },
        onEnter:function () {return stickNavBar(); },
        onExit: function() { $("#nav-general").unstick();}
      })


      .state('home.car',{
        menu:'cars',
        url:'cars/:id',
        views:{
          'header@':{
            templateUrl:'app/views/header.html',
            controller:'fmainController'
          },
          'content@':{
            templateUrl:'app/views/cars/car.html',
            controller:'fCarCtrl'
          },
          'footer@':{
            templateUrl:'app/views/footer.html'
          }
        },
        onEnter:function () {return stickNavBar(); },
        onExit: function() { $("#nav-general").unstick();}
      })




      .state('home.garage',{
        menu:'garage',
        url:'garage',
        views:{
          'header@':{
            templateUrl:'app/views/header.html',
            controller:'fmainController'
          },
          'content@':{
            templateUrl:'app/views/garage/garage.html'
          },
          'footer@':{
            templateUrl:'app/views/footer.html'
          }
        },
        onEnter:function () {return stickNavBar(); },
        onExit: function() { $("#nav-general").unstick();}
        })






/***************************************************************/
/*                    MEDIA                                    */
/***************************************************************/


      .state('home.media',{
        menu:'media',
        url:'media',
        views:{
          'header@':{
            templateUrl:'app/views/header.html',
            controller:'fmainController'
          },
          'content@':{
            templateUrl:'app/views/media/media.html'
          },
          'footer@':{
            templateUrl:'app/views/footer.html'
          }
        },
        onEnter:function () {return stickNavBar(); },
        onExit: function() { $("#nav-general").unstick();}
        })




/***************************************************************/
/*                    CONTACT                                  */
/***************************************************************/

      .state('home.contact',{
        menu:'contact',
        url:'contact',
        views:{
          'header@':{
            templateUrl:'app/views/header.html',
            controller:'fmainController'
          },
          'content@':{
            templateUrl:'app/views/contact/contact.html'
          },
          'footer@':{
            templateUrl:'app/views/footer.html'
          }
        },
        onEnter:function () {return stickNavBar(); },
        onExit: function() { $("#nav-general").unstick();}
        })



/*******************************************/
                //ADMIN
/*******************************************/

      .state('home.login',{
        menu:'login',
        url:'login',
        views:{
        'header@':{
          template :'<br> '
          
          },
          'content@':{
            templateUrl:'app/views/cms/login.html',
            controller:'loginCtrl'
          },
          'footer@':{
            template:''
          }


        }
      })

/*=============== BIKES ====================*/

      .state('home.adminBikes',{
        menu:'adminBikes',
        url:'admin/bikes',
        views:{
          'header@':{
          templateUrl:'app/views/cms/adminNavbar.html'
          
          },
          'content@':{
            templateUrl:'app/views/cms/adminBikes.html',
            controller:'adminbikesCtrl'
          },
          'footer@':{
            template:''
          }
        }
      })



/*=============== BIKE ====================*/

      .state('home.adminBike',{
        menu:'adminBike',
        url:'admin/bike',
        views:{
          'header@':{
          templateUrl:'app/views/cms/adminNavbar.html'
          
          },
          'content@':{
            templateUrl:'app/views/cms/bike.html',
            controller:'createBikeCtrl'
          },
          'footer@':{
            template:''
          }


        }
      })



      .state('home.editBike',{
        menu:'editBike',
        url:'admin/bike/:id',
        views:{
          'header@':{
            templateUrl:'app/views/cms/adminNavbar.html'
          },
          'content@':{
            templateUrl:'app/views/cms/bike.html',
            controller:'editBikeCtrl'
          },
          'footer@':{
            template:''
          }


        }
      })

/*=============== CARS ====================*/

      .state('home.adminCars',{
        menu:'adminCars',
        url:'admin/cars',
        views:{
          'header@':{
          templateUrl:'app/views/cms/adminNavbar.html'
          
          },
          'content@':{
            templateUrl:'app/views/cms/adminCars.html',
            controller:'admincarsCtrl'
          },
          'footer@':{
            template:''
          }
        }
      })



/*=============== CAR ====================*/

      .state('home.adminCar',{
        menu:'adminCar',
        url:'admin/car',
        views:{
          'header@':{
          templateUrl:'app/views/cms/adminNavbar.html'
          
          },
          'content@':{
            templateUrl:'app/views/cms/car.html',
            controller:'createCarCtrl'
          },
          'footer@':{
            template:''
          }


        }
      })



      .state('home.editCar',{
        menu:'editCar',
        url:'admin/car/:id',
        views:{
          'header@':{
            templateUrl:'app/views/cms/adminNavbar.html'
          },
          'content@':{
            templateUrl:'app/views/cms/car.html',
            controller:'editCarCtrl'
          },
          'footer@':{
            template:''
          }


        }
      })




      ; // /$stateProvider

      $locationProvider.html5Mode(true);
      $urlRouterProvider.otherwise('/');
  })



;





var stickNavBar = function(){
  setTimeout(function () {
    $("#nav-general").sticky({
      topSpacing: 0,
      zIndex:10
     });
  },1000)
};
