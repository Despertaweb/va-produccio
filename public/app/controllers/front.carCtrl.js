angular.module('front.carCtrl', [])

.controller('fCarCtrl',['$scope','$stateParams','fCarFactory' ,function($scope,$stateParams, fCarFactory) {

  $scope.car ={};
  fCarFactory.get({
        id:$stateParams.id
      })
      .$promise.then(function (response) {
        $scope.car = response;
      },function (response) {
        $scope.car = "Error: " + response.status + " " + response.statusText;
      });

  

}])
