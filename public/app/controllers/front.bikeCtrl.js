angular.module('front.bikeCtrl', [])

.controller('fBikeCtrl',['$scope','$stateParams','fBikeFactory' ,function($scope,$stateParams, fBikeFactory) {

  $scope.bike ={};
  fBikeFactory.get({
        id:$stateParams.id
      })
      .$promise.then(function (response) {
        $scope.bike = response;
      },function (response) {
        $scope.bike = "Error: " + response.status + " " + response.statusText;
      });

  $scope.par = $stateParams.id;
}])
