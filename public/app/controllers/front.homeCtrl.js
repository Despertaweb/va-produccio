angular.module('front.homeCtrl', [])

.controller('fHomeCtrl',['$scope','preloader','$window', function($scope,preloader,$window) {

    $scope.loaded = false;



      if ( $(window).width() >=991 ){
        $scope.imageLocations = [
                       "../../assets/img/home/vintage_addiction_home_helmet_retina.jpg",
                       "../../assets/img/home/vintage_addiction_home_custom-motocross_retina.jpg",
                       "../../assets/img/home/vintage_addiction_home_jeep-wrangler_retina.jpg",
                       "../../assets/img/home/vintage_addiction_home_yamaha_xj650_retina.jpg"
         ];
      }else {
         $scope.imageLocations = [
                        "../../assets/img/home/vintage_addiction_home_helmet.jpg",
                        "../../assets/img/home/vintage_addiction_home_custom-motocross.jpg",
                        "../../assets/img/home/vintage_addiction_home_jeep-wrangler.jpg",
                        "../../assets/img/home/vintage_addiction_home_yamaha_xj650.jpg"
            ];
      }



    // Preload the images; then, update display when returned.
    preloader.preloadImages( $scope.imageLocations )
 	.then(function() {
    $scope.loaded = true;
  	},
  	function() {
      $scope.loaded = false;
    });


}])
