angular.module('front.carsCtrl', [])

.controller('fCarsCtrl',['$scope','fCarFactory', function($scope,fCarFactory) {

  $scope.cars =[];
   fCarFactory.query(
     function (response) {
       $scope.cars = response;
     },
     function (response) {
       $scope.cars = "ERROR" + response.status+ " "+ response.statusText;
     }
   );

}]);
