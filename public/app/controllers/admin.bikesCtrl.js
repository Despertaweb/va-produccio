angular.module('admin.bikesCtrl', [])

.controller('adminbikesCtrl', ['$scope' ,'BikeFactory', function($scope ,BikeFactory) {

// set a processing variable to show loading things
	$scope.processing = true;

// grab all the bikes at page load
	BikeFactory.query(function(response) {
		$scope.processing = false;

		$scope.bikes =response;
		},
		function(response) {
			$scope.bikes = "ERROR"+ response.status + " "+ response.statusText;
		}

	);

// DELETE a BIKE
	$scope.deleteBike = function(bike_id) {


		$scope.processing = true;

		var deleteQuery = BikeFactory.delete({id:bike_id});
		deleteQuery.$promise.then(function(response) {

			var getAllQuery = BikeFactory.query();
			getAllQuery.$promise.then(function(resp) {
				$scope.processing = false;
		    	$scope.bikes = getAllQuery;
		     // Do whatever when the request is finished	
			})
		});
	}; //deleteBike




}]) //adminBikesCtrl




//======================================================================
//  CREATE BIKE
//======================================================================

.controller('createBikeCtrl', function($scope, BikeFactory,$state, $http) {
	$scope.processing = false;

	$scope.type = 'create';
	
	$scope.bike= {};
    $scope.bike.imgs=[];
    $scope.bike.thumb='';


	BikeFactory.query(function(response) {
		$scope.processing = false;

		$scope.bike._id =response.length+1;
		},
		function(response) {
			$scope.bike._id = "ERROR"+ response.status + " "+ response.statusText;
		}
	);  // query

/* IMAGE Upload */ 


// to Thumbnail
	$scope.uploadThumb = function() {
		var folder = '/va/bikes/'+$scope.bike._id+'/thumbnail/';
		var nameFile =  'Custom_'+$scope.bike.manufacturer+'_'+$scope.bike.model+'_'+$scope.bike._id+'_thumbnail';

		cloudinary.openUploadWidget({
			folder: folder,
			public_id: nameFile,
			cloud_name: 'www-despertaweb-cat', 
			upload_preset: 'thumbBikes_preset',
			theme: 'minimal',
			multiple: false

		}, 
      		function(error, result) { 
				for(var i in result){
					$scope.bike.thumb = result[i].url;
				};
				$scope.$apply();
      		});

	};

// to Gallery
	$scope.uploadImages = function() {
		var folder = '/va/bikes/'+$scope.bike._id;

		cloudinary.openUploadWidget({
			folder: folder,
			cloud_name: 'www-despertaweb-cat', 
			upload_preset: 'bikes_preset',
			theme: 'minimal'
			
			}, 
      		function(error, result) { 
      			console.log('\n\n Result', result);
				for(var i in result){
					$scope.bike.imgs.push(result[i].url);
				};
				$scope.$apply();
      		});

	};





// CREATE new BIKE
	$scope.saveBike = function() {
		$scope.processing = true;

		var createQuery = BikeFactory.save( $scope.bike);
		createQuery.$promise.then(function(resp, err) {
			$scope.processing = false;
			$scope.bike ={};
            $state.go('home.adminBikes');
		})
	};


})



//======================================================================
//  EDIT BIKE
//======================================================================
.controller('editBikeCtrl', function($scope, BikeFactory,$stateParams, $state) {
	$scope.processing = false;

	$scope.type = 'edit';
	$scope.message ='';
	

  $scope.bike ={};
  var newThumb='';

  BikeFactory.get({
        id:$stateParams.id
      })
      .$promise.then(function (response) {
        $scope.bike = response;
      },function (response) {
        $scope.bike = "Error: " + response.status + " " + response.statusText;
      });


// UPDATE a BIKE
      $scope.saveBike = function() {
      	$scope.processing = true;


      	var updateQuery = BikeFactory.update({id: $stateParams.id}, $scope.bike);
      	updateQuery.$promise.then(function(rep) {
      		$scope.processing = false;
      		$scope.message = "Desat amb èxit";
      		setTimeout(function(){ $state.go('home.adminBikes');$scope.bike.message = ''; }, 750);
       	},function(err) {
      		$scope.processing = false;
      		$scope.message = 'Error al Desar, torna a provar-ho.';
      	})
      };


/* IMAGE Upload */ 

// to Thumbnail
	$scope.uploadThumb = function() {
		var folder = '/va/bikes/'+$scope.bike._id+'/thumbnail/';
		var nameFile =  'Custom_'+$scope.bike.manufacturer+'_'+$scope.bike.model+'_'+$scope.bike._id+'_thumbnail_'+Date.now();

		cloudinary.openUploadWidget({
			folder: folder,
			public_id: nameFile,
			cloud_name: 'www-despertaweb-cat', 
			upload_preset: 'thumbBikes_preset',
			theme: 'minimal',
			multiple: false

		}, 
      		function(error, result) { 
				for(var i in result){
					$scope.bike.thumb = result[i].url;
				};
				$scope.$apply();
      		});

	};

// to Gallery
	$scope.uploadImages = function() {
		var folder = '/va/bikes/'+$scope.bike._id;

		cloudinary.openUploadWidget({
			folder: folder,
			cloud_name: 'www-despertaweb-cat', 
			upload_preset: 'bikes_preset',
			theme: 'minimal'
			
		}, 
      		function(error, result) { 
      					for(var i in result){
      						$scope.bike.imgs.push(result[i].url);
       					};
      					$scope.$apply();
      		});

	};







})