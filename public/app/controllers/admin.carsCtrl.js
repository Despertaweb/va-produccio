angular.module('admin.carsCtrl', [])

.controller('admincarsCtrl', ['$scope' ,'CarFactory', function($scope ,CarFactory) {

// set a processing variable to show loading things
	$scope.processing = true;

// grab all the cars at page load
	CarFactory.query(function(response) {
		$scope.processing = false;

		$scope.cars =response;
		},
		function(response) {
			$scope.cars = "ERROR"+ response.status + " "+ response.statusText;
		}

	);

// DELETE a CAR
	$scope.deleteCar = function(car_id) {


		$scope.processing = true;

		var deleteQuery = CarFactory.delete({id:car_id});
		deleteQuery.$promise.then(function(response) {

			var getAllQuery = CarFactory.query();
			getAllQuery.$promise.then(function(resp) {
				$scope.processing = false;
		    	$scope.cars = getAllQuery;
		     // Do whatever when the request is finished	
			})
		});
	}; //deleteCar




}]) //adminCarsCtrl




//======================================================================
//  CREATE CAR
//======================================================================

.controller('createCarCtrl', function($scope, CarFactory,$state, $http) {
	$scope.processing = false;

	$scope.type = 'create';
	
	$scope.car= {};
    $scope.car.imgs=[];
    $scope.car.thumb='';


	CarFactory.query(function(response) {
		$scope.processing = false;

		$scope.car._id =response.length+1;
		},
		function(response) {
			$scope.car._id = "ERROR"+ response.status + " "+ response.statusText;
		}
	);  // query

/* IMAGE Upload */ 


// to Thumbnail
	$scope.uploadThumb = function() {
		var folder = '/va/cars/'+$scope.car._id+'/thumbnail/';
		var nameFile =  'Custom_'+$scope.car.manufacturer+'_'+$scope.car.model+'_'+$scope.car._id+'_thumbnail';

		cloudinary.openUploadWidget({
			folder: folder,
			public_id: nameFile,
			cloud_name: 'www-despertaweb-cat', 
			upload_preset: 'thumbCars_preset',
			theme: 'minimal',
			multiple: false

		}, 
      		function(error, result) { 
				for(var i in result){
					$scope.car.thumb = result[i].url;
				};
				$scope.$apply();
      		});

	};

// to Gallery
	$scope.uploadImages = function() {
		var folder = '/va/cars/'+$scope.car._id;

		cloudinary.openUploadWidget({
			folder: folder,
			cloud_name: 'www-despertaweb-cat', 
			upload_preset: 'cars_preset',
			theme: 'minimal'
			
			}, 
      		function(error, result) { 
      			console.log('\n\n Result', result);
				for(var i in result){
					$scope.car.imgs.push(result[i].url);
				};
				$scope.$apply();
      		});

	};





// CREATE new CAR
	$scope.saveCar = function() {
		$scope.processing = true;

		var createQuery = CarFactory.save( $scope.car);
		createQuery.$promise.then(function(resp, err) {
			$scope.processing = false;
			$scope.car ={};
            $state.go('home.adminCars');
		})
	};


})



//======================================================================
//  EDIT CAR
//======================================================================
.controller('editCarCtrl', function($scope, CarFactory,$stateParams, $state) {
	$scope.processing = false;

	$scope.type = 'edit';
	$scope.message ='';
	

  $scope.car ={};
  var newThumb='';

  CarFactory.get({
        id:$stateParams.id
      })
      .$promise.then(function (response) {
        $scope.car = response;
      },function (response) {
        $scope.car = "Error: " + response.status + " " + response.statusText;
      });


// UPDATE a CAR
      $scope.saveCar = function() {
      	$scope.processing = true;


      	var updateQuery = CarFactory.update({id: $stateParams.id}, $scope.car);
      	updateQuery.$promise.then(function(rep) {
      		$scope.processing = false;
      		$scope.message = "Desat amb èxit";
      		setTimeout(function(){ $state.go('home.adminCars');$scope.car.message = ''; }, 750);
       	},function(err) {
      		$scope.processing = false;
      		$scope.message = 'Error al Desar, torna a provar-ho.';
      	})
      };


/* IMAGE Upload */ 

// to Thumbnail
	$scope.uploadThumb = function() {
		var folder = '/va/cars/'+$scope.car._id+'/thumbnail/';
		var nameFile =  'Custom_'+$scope.car.manufacturer+'_'+$scope.car.model+'_'+$scope.car._id+'_thumbnail_'+Date.now();

		cloudinary.openUploadWidget({
			folder: folder,
			public_id: nameFile,
			cloud_name: 'www-despertaweb-cat', 
			upload_preset: 'thumbCars_preset',
			theme: 'minimal',
			multiple: false

		}, 
      		function(error, result) { 
				for(var i in result){
					$scope.car.thumb = result[i].url;
				};
				$scope.$apply();
      		});

	};

// to Gallery
	$scope.uploadImages = function() {
		var folder = '/va/cars/'+$scope.car._id;

		cloudinary.openUploadWidget({
			folder: folder,
			cloud_name: 'www-despertaweb-cat', 
			upload_preset: 'cars_preset',
			theme: 'minimal'
			
		}, 
      		function(error, result) { 
      					for(var i in result){
      						$scope.car.imgs.push(result[i].url);
       					};
      					$scope.$apply();
      		});

	};







})