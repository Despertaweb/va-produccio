angular.module('frontApp', [

		'ngAnimate',
		'ngResource',


		'front.routes',

		// 
		//=========  CONTROLLERS  =================
		//
		
		// MAIN
		'front.mainCtrl',
		// HOME
		'front.homeCtrl',
		// CAR & BIKE by Id
		'front.bikeCtrl',
		'front.carCtrl',
		
		'front.bikesCtrl',
		'front.carsCtrl',
		// USERS
		'userCtrl',

		//======= CMS ============\\

		'admin.bikesCtrl',
		'admin.carsCtrl',
		
		// 
		//=========  SERVICES  =================
		//

		//CARS & BIKES
		'front.bikeService',
		'front.carService',
		//PRELOADING
		'preloader',
		//AUTH
		'authService',



		//DIRECTIVES
		'directives',

		//OTHER
		'jkuri.gallery'

])


// application configuration to integrate token into requests
.config(function($httpProvider) {
	
// console.log('\n\n\n-------->  Interceptor <------\n\n\n');
//  attach our auth interceptor to the http requests
	$httpProvider.interceptors.push('httpInterceptor');






})
// .constant('baseUrl', 'http://localhost:8080/')
// .constant('baseUrl', 'https://va-produccio.herokuapp.com/')
.constant('baseUrl', '')



;
