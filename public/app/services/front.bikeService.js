angular.module('front.bikeService', ['ngResource'])


.factory('fBikeFactory', ['$resource', 'baseUrl',function ($resource, baseUrl) {

  
    return $resource(baseUrl+'public/bikes/:id',null,{
      'update':{
        method:'PUT'
      }
    });



}])


.factory('BikeFactory', ['$resource', 'baseUrl',function ($resource, baseUrl) {

  
    return $resource(baseUrl+'api/bikes/:id',null,{
      'update':{
        method:'PUT'
      }
    });



}])

;
