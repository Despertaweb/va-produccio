angular.module('userService',['ngResource'])
	.factory('userFactory',['$resource',function($resource) {

    // Local Server
		return $resource(baseUrl+'api/:id',{id:"@id"},{
		    update: {
		      method: 'PUT' // this method issues a PUT request
		    }
		})

  // Heroku Server
/*
		return $resource('https://test-vintage-api.herokuapp.com/api/users/:id',{id:"@id"},{
		    update: {
		      method: 'PUT' // this method issues a PUT request
		    }
		})
	
*/
	}])