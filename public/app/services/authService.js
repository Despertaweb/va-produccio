angular.module('authService', [])

// .constant('baseUrl', 'http://localhost:8080/')

.factory('AutFactory', ['$resource', 'baseUrl', '$localStorage','$state', '$rootScope', 
	function ($resource, baseUrl, $localStorage,$state,$rootScope) {
	
	var autFac = {};

	autFac.login = function(loginData) {

		$resource(baseUrl+"api/login")
		.save(loginData,
			function(res) {
				$localStorage.store('token', res.token);
				
				$rootScope.isLogged=true;
				$state.go('home.adminBikes');
			},function(err) {
				console.log('err : ', err.data.err);
			})
	}




	return autFac;
}])








.factory('$localStorage', ['$window', function ($window ) {
    return {
        store: function (key, value) {
            $window.localStorage[key] = value;
        },
        get: function (key, defaultValue) {
            return $window.localStorage[key] || defaultValue;
        },
        remove: function (key) {
            $window.localStorage.removeItem(key);
        },
        storeObject: function (key, value) {
            $window.localStorage[key] = JSON.stringify(value);
        },
        getObject: function (key, defaultValue) {
            return JSON.parse($window.localStorage[key] || defaultValue);
        }
    }
}])


.factory('httpInterceptor', function  ($q, $location, $localStorage) {

	var interceptorFactory = {};

	// this will happen on all HTTP requests
	interceptorFactory.request = function(req) {

		// grab the token
		var token = $localStorage.get('token');

		// if the token exists, add it to the header as x-access-token
		if (token) {

			req.headers['x-access-token'] = token;
		}
		return req;
	};

	// happens on response errors
	interceptorFactory.responseError = function(response) {

		// if our server returns a 403 forbidden response
		if (response.status == 403) {
			$localStorage.remove('token');
			$location.path('/login');
		}

		// return the errors from the server as a promise
		return $q.reject(response);
	};

	return interceptorFactory;



});