angular.module('front.carService', ['ngResource'])


.factory('fCarFactory', ['$resource','baseUrl',function ($resource,baseUrl) {

    return $resource(baseUrl+'public/cars/:id',null,{
      'update':{
        method:'PUT'
      }
    });
}])


.factory('CarFactory', ['$resource','baseUrl',function ($resource,baseUrl) {

    return $resource(baseUrl+'api/cars/:id',null,{
      'update':{
        method:'PUT'
      }
    });
}])




;
