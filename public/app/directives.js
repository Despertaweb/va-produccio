
angular.module('directives', ['ngResource'])


/* XARXES SOCIALS*/
.directive('social', function() {
  return {
    templateUrl: 'app/views/directives/social.html'
  };
})


/* TOP MENU */
.directive('topmenu', function() {
  return {
    templateUrl: 'app/views/directives/topmenu.html'
  };
})



/* CAPÇALERA */
.directive('innerheader', function() {
  return {
    templateUrl: 'app/views/directives/inner-header.html'
  };
})

/* PEU */
.directive('innerfooter', function() {
  return {
    templateUrl: 'app/views/footer.html'
  };
})


/* To top Button */
.directive('backtotop', function() {
  return {
    restrict: "E",
    templateUrl: 'app/views/directives/topbutton.html',
    link:function () {
      $('#back-to-top').click(function () {

          $('#back-to-top').tooltip('hide');
          $('body,html').animate({
              scrollTop: 0
          }, 800);
          return false;
      });
    }
  };
})


/* LOADING */

.directive  ('loading', function () {
  return{
    templateUrl:'app/views/directives/loading.html'
  };
})


/** PRECARREGA D' IMATGES A background-image  **/

.directive('background', function($q) {
    return {
      restrict: 'E',
      link: function(scope, element, attrs, tabsCtrl) {
        scope.preload = function(url) {
          var deffered = $q.defer(),
          image = new Image();

          image.src = url;

          if (image.complete) {

            deffered.resolve();

          } else {

            image.addEventListener('load', function() {
              deffered.resolve();
            });

            image.addEventListener('error', function() {
              deffered.reject();
            });
          }

          return deffered.promise;
        }

        element.hide();

        scope.preload(attrs.url).then(function(){
          element.css({
            "background-image": "url('" + attrs.url + "')"
          });
          element.fadeIn();
        })

      }
    };
  });

;
