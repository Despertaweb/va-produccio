var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var browserSync = require('browser-sync');
var concat = require('gulp-concat');



var assets = './public/assets';

var img_src = './public/assets/img/home/*', 
    img_dest = './public/assets/img/CompImages';



//Default
gulp.task('default' /*['scripts'] */ );


// JS
gulp.task('scripts', function() {
  return gulp.src([


      // assets + '/js/app.js',
      assets + '/js/**/*.js'

    ])
    // .pipe(concat('app.js'))
    .pipe(gulp.dest('./public/assets/js'))

})




// AUTORELOAD
gulp.task('watch', ['browser-sync'], function() {

  gulp.watch(assets + '/**/*.css');
  gulp.watch(assets + '/**/*.js');

})

gulp.task('browser-sync',/* ['scripts'],*/ function() {
  var files = [
    './public/**/*.html',
    './public/**/css/**/*.css',
    './public/**/img/**/*',
    './public/**/js/**/*.js'
  ];

  browserSync.init(files, {
    server: {
      baseDir: "./public",
    },
    port:"8888"
  });
  // Watch any files in dist/, reload on change
  gulp.watch(['public/**']).on('change', browserSync.reload);

});
